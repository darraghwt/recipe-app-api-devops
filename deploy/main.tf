provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "recipe-app-api-devops-tfstate-seanchara"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Enviroment = terraform.workspace
    Project    = var.project
    Owner      = var.contact
    ManagedBy  = "Terraform"
  }
}
